<?php

namespace Novalnet\Bundle\NovalnetBundle\PaymentMethod\Config\Provider;

use Novalnet\Bundle\NovalnetBundle\PaymentMethod\Config\NovalnetSofortConfigInterface;

/**
 * Interface for config provider of Novalnet payment method
 */
interface NovalnetSofortConfigProviderInterface extends NovalnetConfigProviderInterface
{
    /**
     * @return NovalnetSofortConfigInterface[]
     */
    public function getPaymentConfigs();

    /**
     * @param string $identifier
     *
     * @return NovalnetSofortConfigInterface|null
     */
    public function getPaymentConfig($identifier);
}
