<?php

namespace Novalnet\Bundle\NovalnetBundle\PaymentMethod\Config\Provider;

use Novalnet\Bundle\NovalnetBundle\PaymentMethod\Config\NovalnetGlobalConfigInterface;

/**
 * Interface for config provider of Novalnet payment method
 */
interface NovalnetGlobalConfigProviderInterface extends NovalnetConfigProviderInterface
{
    /**
     * @return NovalnetGlobalConfigInterface[]
     */
    public function getPaymentConfigs();

    /**
     * @param string $identifier
     *
     * @return NovalnetGlobalConfigInterface|null
     */
    public function getPaymentConfig($identifier);
}
