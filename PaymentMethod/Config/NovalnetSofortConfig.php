<?php

namespace Novalnet\Bundle\NovalnetBundle\PaymentMethod\Config;

/**
 * Configuration class which is used to get specific configuration for Novalnet payment method
 * Usually it has additional get methods for payment type specific configurations
 */
class NovalnetSofortConfig extends NovalnetConfig implements NovalnetSofortConfigInterface
{
    const ADMIN_LABEL_KEY = 'Sofort';
    const ADMIN_LABEL_SUFFIX = 'SOFORT';
    const SOFORT_TESTMODE = 'sofort_testmode';
    const SOFORT_NOTIFICATION = 'sofort_buyer_notification';

    /**
     * {@inheritDoc}
     */
    public function getTestMode()
    {
        return (int)$this->get(self::SOFORT_TESTMODE);
    }

    /**
     * {@inheritDoc}
     */
    public function getBuyerNotification()
    {
        return (string)$this->get(self::SOFORT_NOTIFICATION);
    }
}
