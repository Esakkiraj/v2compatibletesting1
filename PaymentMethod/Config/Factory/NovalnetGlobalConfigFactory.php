<?php

namespace Novalnet\Bundle\NovalnetBundle\PaymentMethod\Config\Factory;

use Novalnet\Bundle\NovalnetBundle\Entity\NovalnetSettings;
use Novalnet\Bundle\NovalnetBundle\PaymentMethod\Config\NovalnetGlobalConfig;
use Novalnet\Bundle\NovalnetBundle\PaymentMethod\Config\NovalnetConfig;

/**
 * Creates instances of configurations for Novalnet payment method
 */
class NovalnetGlobalConfigFactory extends NovalnetConfigFactory implements NovalnetGlobalConfigFactoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function createConfig(NovalnetSettings $settings)
    {
        $channel = $settings->getChannel();
        $params = [
           NovalnetGlobalConfig::FIELD_PAYMENT_METHOD_IDENTIFIER => $this->getPaymentMethodIdentifier($channel),
           NovalnetGlobalConfig::TARIFF => $settings->getTariff(),
           NovalnetGlobalConfig::PRODUCT_ACTIVATION_KEY => $settings->getProductActivationKey(),
           NovalnetGlobalConfig::PAYMENT_ACCESS_KEY => $settings->getPaymentAccessKey(),
           NovalnetGlobalConfig::CALLBACK_TESTMODE => $settings->getCallbackTestMode(),
           NovalnetGlobalConfig::CALLBACK_EMAILTO => $settings->getCallbackEmailTo(),
           NovalnetGlobalConfig::DISPLAY_PAYMENT_LOGO => $settings->getDisplayPaymentLogo(),

        ];

        return new NovalnetGlobalConfig($params);
    }
}
