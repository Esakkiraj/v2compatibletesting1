<?php

namespace Novalnet\Bundle\NovalnetBundle\PaymentMethod\Config\Factory;

use Novalnet\Bundle\NovalnetBundle\Entity\NovalnetSettings;
use Novalnet\Bundle\NovalnetBundle\PaymentMethod\Config\NovalnetSofortConfig;
use Novalnet\Bundle\NovalnetBundle\PaymentMethod\Config\NovalnetConfig;

/**
 * Creates instances of configurations for Novalnet payment method
 */
class NovalnetSofortConfigFactory extends NovalnetConfigFactory implements
    NovalnetSofortConfigFactoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function createConfig(NovalnetSettings $settings)
    {
        $channel = $settings->getChannel();
        $params = [
          NovalnetSofortConfig::FIELD_PAYMENT_METHOD_IDENTIFIER => $this->getPaymentMethodIdentifier($channel),
          NovalnetSofortConfig::FIELD_ADMIN_LABEL =>
                $this->getAdminLabel($channel, NovalnetSofortConfig::ADMIN_LABEL_SUFFIX),
          NovalnetSofortConfig::FIELD_LABEL => $settings->getSofortLabel(),
          NovalnetSofortConfig::FIELD_SHORT_LABEL =>
                $settings->getSofortShortLabel(),
          NovalnetSofortConfig::TARIFF => $settings->getTariff(),
          NovalnetSofortConfig::PRODUCT_ACTIVATION_KEY => $settings->getProductActivationKey(),
          NovalnetSofortConfig::PAYMENT_ACCESS_KEY => $settings->getPaymentAccessKey(),
          NovalnetSofortConfig::SOFORT_TESTMODE => $settings->getSofortTestMode(),
          NovalnetSofortConfig::SOFORT_NOTIFICATION => $settings->getSofortBuyerNotification(),
          NovalnetSofortConfig::CALLBACK_TESTMODE => $settings->getCallbackTestMode(),
          NovalnetSofortConfig::CALLBACK_EMAILTO => $settings->getCallbackEmailTo(),
          NovalnetSofortConfig::DISPLAY_PAYMENT_LOGO => $settings->getDisplayPaymentLogo(),
        ];

        return new NovalnetSofortConfig($params);
    }
}
