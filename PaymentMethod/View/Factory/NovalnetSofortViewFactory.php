<?php

namespace Novalnet\Bundle\NovalnetBundle\PaymentMethod\View\Factory;

use Novalnet\Bundle\NovalnetBundle\PaymentMethod\Config\NovalnetSofortConfigInterface;
use Novalnet\Bundle\NovalnetBundle\PaymentMethod\View\NovalnetSofortView;

/**
 * Novalnet payment method view factory
 */
class NovalnetSofortViewFactory extends NovalnetViewFactory implements NovalnetSofortViewFactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function create(NovalnetSofortConfigInterface $config)
    {
        return new NovalnetSofortView(
            $this->formFactory,
            $this->novalnetHelper,
            $this->translator,
            $this->doctrine,
            $this->userLocalizationManager,
            $config
        );
    }
}
