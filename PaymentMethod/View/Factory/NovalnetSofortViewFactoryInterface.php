<?php

namespace Novalnet\Bundle\NovalnetBundle\PaymentMethod\View\Factory;

use Novalnet\Bundle\NovalnetBundle\PaymentMethod\Config\NovalnetSofortConfigInterface;
use Oro\Bundle\PaymentBundle\Method\View\PaymentMethodViewInterface;

/**
 * Novalnet payment method view provider factory interface
 */
interface NovalnetSofortViewFactoryInterface
{
    /**
     * @param NovalnetSofortConfigInterface $config
     * @return PaymentMethodViewInterface
     */
    public function create(NovalnetSofortConfigInterface $config);
}
