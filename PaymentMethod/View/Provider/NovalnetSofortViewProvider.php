<?php

namespace Novalnet\Bundle\NovalnetBundle\PaymentMethod\View\Provider;

use Novalnet\Bundle\NovalnetBundle\PaymentMethod\Config\NovalnetSofortConfigInterface;
use Novalnet\Bundle\NovalnetBundle\PaymentMethod\Config\Provider\NovalnetSofortConfigProviderInterface;
use Novalnet\Bundle\NovalnetBundle\PaymentMethod\View\Factory\NovalnetSofortViewFactoryInterface;
use Oro\Bundle\PaymentBundle\Method\View\AbstractPaymentMethodViewProvider;

/**
 * Novalnet payment method view provider factory
 */
class NovalnetSofortViewProvider extends AbstractPaymentMethodViewProvider
{
    /** @var NovalnetSofortViewFactoryInterface */
    private $factory;

    /** @var NovalnetSofortConfigProviderInterface */
    private $configProvider;

    /**
     * @param NovalnetSofortViewFactoryInterface $factory
     * @param NovalnetSofortConfigProviderInterface $configProvider
     */
    public function __construct(
        NovalnetSofortViewFactoryInterface $factory,
        NovalnetSofortConfigProviderInterface $configProvider
    ) {
        $this->factory = $factory;
        $this->configProvider = $configProvider;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function buildViews()
    {
        $configs = $this->configProvider->getPaymentConfigs();
        foreach ($configs as $config) {
            $this->addNovalnetSofortView($config);
        }
    }

    /**
     * @param NovalnetSofortConfigInterface $config
     */
    protected function addNovalnetSofortView(NovalnetSofortConfigInterface $config)
    {
        $this->addView(
            $config->getPaymentMethodIdentifier(),
            $this->factory->create($config)
        );
    }
}
