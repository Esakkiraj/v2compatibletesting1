<?php

namespace Novalnet\Bundle\NovalnetBundle\PaymentMethod\Provider;

use Novalnet\Bundle\NovalnetBundle\PaymentMethod\Config\NovalnetSofortConfigInterface;
use Novalnet\Bundle\NovalnetBundle\PaymentMethod\Config\Provider\NovalnetSofortConfigProviderInterface;
use Novalnet\Bundle\NovalnetBundle\PaymentMethod\Factory\NovalnetSofortPaymentMethodFactoryInterface;
use Oro\Bundle\PaymentBundle\Method\Provider\AbstractPaymentMethodProvider;

/**
 * Provider for retrieving configured payment method instances
 */
class NovalnetSofortMethodProvider extends AbstractPaymentMethodProvider
{
    /**
     * @var NovalnetSofortPaymentMethodFactoryInterface
     */
    protected $factory;

    /**
     * @var NovalnetSofortConfigProviderInterface
     */
    private $configProvider;

    /**
     * @param NovalnetSofortConfigProviderInterface $configProvider
     * @param NovalnetSofortPaymentMethodFactoryInterface $factory
     */
    public function __construct(
        NovalnetSofortConfigProviderInterface $configProvider,
        NovalnetSofortPaymentMethodFactoryInterface $factory
    ) {
        parent::__construct();

        $this->configProvider = $configProvider;
        $this->factory = $factory;
    }

    /**
     * {@inheritdoc}
     */
    protected function collectMethods()
    {
        $configs = $this->configProvider->getPaymentConfigs();

        foreach ($configs as $config) {
            $this->addNovalnetSofortPaymentMethod($config);
        }
    }

    /**
     * @param NovalnetSofortConfigInterface $config
     */
    protected function addNovalnetSofortPaymentMethod(NovalnetSofortConfigInterface $config)
    {
        $this->addMethod(
            $config->getPaymentMethodIdentifier(),
            $this->factory->create($config)
        );
    }
}
