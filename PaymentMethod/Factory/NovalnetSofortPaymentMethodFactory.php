<?php

namespace Novalnet\Bundle\NovalnetBundle\PaymentMethod\Factory;

use Novalnet\Bundle\NovalnetBundle\PaymentMethod\NovalnetSofortPaymentMethod;
use Novalnet\Bundle\NovalnetBundle\PaymentMethod\Config\NovalnetSofortConfigInterface;
use Oro\Bundle\PaymentBundle\Method\PaymentMethodInterface;

/**
 * Factory allows to create Novalnet payment method, using
 * dynamic property NovalnetSofortConfigInterface $config
 * NovalnetSofortPaymentMethod depends on this dynamic evaluated config
 * so it can not be defined directly in di and requires factory to create it
 */
class NovalnetSofortPaymentMethodFactory extends NovalnetPaymentMethodFactory implements
    NovalnetSofortPaymentMethodFactoryInterface
{
    /**
     * @param NovalnetSofortConfigInterface $config
     *
     * @return PaymentMethodInterface
     */
    public function create(NovalnetSofortConfigInterface $config)
    {
        return new NovalnetSofortPaymentMethod(
            $config,
            $this->routerInterface,
            $this->doctrineHelper,
            $this->novalnetHelper,
            $this->requestStack,
            $this->translator,
            $this->userLocalizationManager,
            $this->client,
            $this->paymentMethodProvider
        );
    }
}
